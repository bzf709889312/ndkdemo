package com.bzf.ndkdemo;

public class NDKUtils {

    static {
        System.loadLibrary("native-lib");
    }

    public native byte[] stringToBytes(String content);

    public native String bytesToString(byte[] bytes);
}
