package com.bzf.ndkdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView tv = (TextView) findViewById(R.id.sample_text);


        NDKUtils ndkUtils = new NDKUtils();
        byte[] hellos = ndkUtils.stringToBytes("Hello");
        String s = ndkUtils.bytesToString(hellos);
        tv.setText(hellos.length+":"+s);
    }

}
