#include <jni.h>
#include <string>


JNIEXPORT jint JNI_OnLoad(JavaVM* vm,void* reserved);
JNIEXPORT jstring JNICALL bytes_to_string(JNIEnv *env,jobject obj,jbyteArray bytes);

JNIEXPORT jbyteArray JNICALL string_to_bytes(JNIEnv *env,jobject obj,jstring content) {
    jboolean isCopy;
    const char *string = env->GetStringUTFChars(content, &isCopy);
    jbyteArray pArray = env->NewByteArray(sizeof(string));
    env->SetByteArrayRegion(pArray, 0, sizeof(string), reinterpret_cast<const jbyte *>(string));
    return pArray;
}



jstring bytes_to_string(JNIEnv *env, jobject obj, jbyteArray bytes) {
    jboolean isCopy;
    const char * content = reinterpret_cast<const char *>(env->GetByteArrayElements(bytes, &isCopy));
    return env->NewStringUTF(content);
}


/*定义函数映射表*/
static JNINativeMethod method_table[] = {
        {"stringToBytes","(Ljava/lang/String;)[B",(void *)string_to_bytes},
        {"bytesToString","([B)Ljava/lang/String;",(void *)bytes_to_string}
};


static int registerNativeMethods(JNIEnv *env, const char *className, JNINativeMethod *methods,
                                 int numberMethods) {
    jclass clazz;
    clazz = (*env).FindClass(className);
    if(clazz==NULL){
        return JNI_FALSE;
    }

    if((*env).RegisterNatives(clazz,methods,numberMethods)<0){
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

static int registerNatives(JNIEnv* env){
    /*指定注册的类*/
    const char *className = "com/bzf/ndkdemo/NDKUtils";
    return registerNativeMethods(env,className,method_table, sizeof(method_table)/ sizeof(method_table[0]));
}


jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    JNIEnv* env = NULL;
    jint result = -1;
    if((*vm).GetEnv((void **)&env, JNI_VERSION_1_4)!=JNI_OK){
        return -1;
    }

    /*注册*/
    if(!registerNatives(env)){
        return -1;
    }

    result = JNI_VERSION_1_4;

    return result;
}

